---
title: 15.1. Aantal wooneenheden
---
## Indicator

**Definitie:** Het aantal wooneenheden waarover een zorgaanbieder per vestiging en op organisatieniveau op een peildatum beschikt.

**Teller:** Aantal wooneenheden.

**Noemer:** Niet van toepassing.


## Toelichting

Deze indicator geeft de capaciteit van een zorgaanbieder op een bepaald moment aan. De capaciteit betreft het aantal wooneenheden dat geschikt is voor personen, zoals bijvoorbeeld cliënten en eventueel een partner. 

Deze indicator wordt op een peildatum op organisatieniveau en per vestiging berekend. 


## Uitgangspunten

* Het betreft wooneenheden waarin een of meerdere personen kunnen wonen, zoals cliënten en eventueel een partner. 
* Een wooneenheid is het samenstel van één, of meerdere kamer of ruimtes (onz-g:Room), die samen geschikt zijn voor bewoning en geen kleinere wooneenheden bevat. Een wooneenheid betreft dus de kleinste eenheid geschikt voor bewoning. Een lokatie met meerdere wooneenheden wordt op zichzelf dus niet beschouwd als een wooneenheid, ook al biedt die lokatie wel de mogelijkheid om er te wonen. De nadruk bij dit concept ligt derhalve op het begrip ’–eenheid’.
* Indicatoren uit de reeks 15 en 16 in het uitwisselprofiel van de zorgkantoren beschrijven de capaciteit en de bezetting in wooneenheden en personen. Voor deze indicatoren geldt het volgende. Capaciteit en bezetting worden niet gecombineerd in één indicator. De capaciteit kan uitgedrukt worden in ‘aantallen wooneenheden’ of ‘aantallen cliënten’. Beide per vestiging en op organisatieniveau. Deze aantallen worden bepaald o.b.v. de registratie in het Electronisch Cliënten Dossier: aantal woonheden bij een vestiging en per wooneenheid het aantal personen. Het is niet mogelijk om capaciteit uit te drukken in aantallen cliënten (of personen) met bepaalde kenmerken, zoals diagnose, zorgprofiel, etc. Dit geldt ook voor ‘partners’ van cliënten. De bezetting kan uitgedrukt worden in ‘aantallen wooneenheden waarin cliënten op dat moment verblijven’ of ‘aantallen cliënten met specifieke kenmerken', zoals indicatie, leveringsvorm of PG/somatiek (o.b.v. bv. zorgprofiel).


## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle wooneenheden op de peildatum.  
2. Bepaal per wooneenheid de vestiging.
3. Bereken het aantal wooneenheden per vestiging en voor de totale organisatie.

Peildatum: dd-mm-jjjj 
| Organisatieonderdeel | Aantal wooneenheden | 
|----------------|--------|
| Totaal organisatie | Stap 3 | 
| Vestiging 1 | Stap 3 | 
| Vestiging 2 | Stap 3 | 
| Vestiging N | Stap 3 | 
