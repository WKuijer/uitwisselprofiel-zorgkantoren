---
title: 6.1. Percentage ingezette uren personeel per kwalificatieniveau
---
## Indicator

**Definitie:** Aantal over de periode ingezette uren met kwalificatieniveau X gedeeld door het totaal aantal ingezette uren per kwartaal.

**Teller:** Aantal ingezette uren met kwalificatieniveau X per kwartaal.

**Noemer:** Aantal ingezette uren per kwartaal.

## Toelichting
Deze indicator betreft de verhouding tussen het aantal ingezette uren van personeel in loondienst op een bepaald kwalificatieniveau ten opzichte van het totaal aantal ingezette uren voor alle kwalificatieniveaus samen (en ook per vestiging). De medewerkers dienen zorgverlener te zijn. Op basis van de ingezette uren kan het aantal ingezette fte per kwalificatieniveau worden berekend. Voor de berekening van ingezette uren (gewerkte tijd in uren) worden meeruren, overuren en oproepuren meegerekend.

Op de (deel)resultaten van deze indicator kan een verdeelsleutel worden toegepast.


## Uitgangspunten
* Per vestiging en voor de gehele organisatie.
* Personen die tegelijkertijd over meerdere arbeidsovereenkomsten beschikken, tellen mee in de berekening wanneer de uren geregistreerd zijn als zorgverlener.
* Om deze indicator te kunnen berekenen dienen de ingezette uren (gewerkte tijd in uren) beschikbaar te zijn vanuit de urenregistratie van de zorgaanbieder.
* De geregistreerde ingezette uren, zijn uren exclusief verzuim en (zwangerschaps-)verlof en inclusief meeruren, overuren en oproepuren.
* Op basis van de gewenste eenheid (bijv. 36 uur/week) kan het aantal fte worden berekend (bijvoorbeeld 36 uren per week is gelijk aan 1 fte).
* Toebedeling van ingezette uren naar een vestiging is op basis van de vestiging in de urenregistratie. Indien deze niet beschikbaar is worden de uren toebedeeld aan de vestiging gerelateerd aan de arbeidsovereenkomst. 


## Berekening

Deze indicator wordt als volgt berekend:
1.  Selecteer alle personeelsleden in loondienst die zorgverlener zijn.
2.  Tel per persoon het aantal ingezette uren (gewerkte tijd in uren) dat de persoon heeft geregistreerd in de verslagperiode per vestiging.
3.  Bereken op basis van stap 2 het totaal aantal uren door alle uren bij elkaar op te tellen.
4.  Bereken het percentage aan ingezette uren per vestiging door alle uren van die vestiging bij elkaar op te tellen en op basis van stap 3 te delen door het totaal aantal uren en te vermenigvuldigen met 100%.
5.  Bereken op basis van stap 3 voor elk kwalificatieniveau het aantal uren.
6.  Bereken per kwalificatieniveau per vestiging het percentage door de uren per niveau per vestiging bij elkaar op te tellen en te delen door het totaal aantal uren van de organisatie en te vermenigvuldigen met 100%.


| Periode:     |  Ingezette uren aan zorgverleners | Percentage Vestiging 1 | Percentage Vestiging 2 | Percentage Vestiging N |
|----------------|--------|--------|--------|--------|
| Alle kwalificatieniveaus | Stap 3 | Stap 4 | Stap 4 | Stap 4 |
| Kwalificatieniveau 1 | Stap 5 | Stap 6 | Stap 6 | Stap 6 |
| Kwalificatieniveau 2 | Stap 5 | Stap 6 | Stap 6 | Stap 6 |
| Kwalificatieniveau N | Stap 5 | Stap 6 | Stap 6 | Stap 6 |
