---
title: 1.2. Aantal personeelsleden op een peildatum
---
## Indicator

**Definitie:** Het aantal personeelsleden op een peildatum per vestiging per zorg- en niet-zorg gerelateerde functies per financieringsstroom.

**Teller:** Het aantal personeelsleden op een peildatum.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator betreft het aantal personeelsleden op een peildatum. 

Deze indicator wordt berekend op organisatieniveau, per vestiging en per zorg en niet-zorg gerelateerde functie. Ook worden per categorie totalen berekend.

Op de (deel)resultaten van deze indicator kan een verdeelsleutel worden toegepast.


## Uitgangspunten

Op deze indicator zijn de volgende Uitgangspunten van toepassing:

* Alle personeelsleden (zorg- en niet-zorg) worden geïncludeerd.
* Per vestiging en voor de gehele organisatie.
* Op een peildatum.


**Voorbeeld**

Onderstaande tabel beschrijft een voorbeeld van de manier waarop een medewerker meetelt in elk van de categorieen. Deze medewerker beschikte op de peildatum over 2 werkovereenkomsten. De medewerker was zorgverlener en manager. 

| Peildatum:     |  Zorg  | Niet-zorg | Totaal |
|----------------|--------|-----------|--------|
| Totaal organisatie |  1     | 1         | 1      |
| Vestiging 1      |  1     | 1         | 1      |
| Vestiging 2      |  -     | -         | -      |
| Vestiging N      |  -     | -         | -      |


## Berekening

Deze indicator wordt als volgt berekend (zie tevens onderstaande tabel):

1. Selecteer alle personen die op de peildatum personeelslid waren.  
(De begindatum van de werkovereenkomst ligt voor of op de peildatum en de einddatum ligt niet op de peildatum).
2. Bepaal o.b.v. de functie in de werkovereenkomst voor elke persoon uit stap 1 of deze persoon zorg of niet-zorg gerelateerd was.
3. Bepaal o.b.v. de werkovereenkomst voor elke persoon uit stap 2 de vestiging.
4. Bereken o.b.v. stap 3 het aantal personen en groepeer per vestiging, per vestiging voor zorg- en niet-zorg en voor de totale organisatie.


| Periode:       |  Zorg  | Niet-zorg | Totaal |
|----------------|--------|-----------|--------|
| Totaal organisatie | Stap 4 | Stap 4    | Stap 4 |
| Vestiging 1      | Stap 4 | Stap 4    | Stap 4 |
| Vestiging 2      | Stap 4 | Stap 4    | Stap 4 |
| Vestiging N      | Stap 4 | Stap 4    | Stap 4 |
