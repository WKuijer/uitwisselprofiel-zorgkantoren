---
title: 4.1. Gemiddelde contractomvang personeel
---
## Indicator

**Definitie:** Aantal contractuele uren aan personeelsleden met een arbeidsovereenkomst in het kwartaal gedeeld door het gemiddeld aantal personeelsleden met een arbeidsovereenkomst in het kwartaal.

**Teller:** Aantal contractuele uren aan personeelsleden met een arbeidsovereenkomst in het kwartaal.

**Noemer:** Gemiddeld aantal personeelsleden met een arbeidsovereenkomst in het kwartaal.

## Toelichting
Deze indicator betreft het aantal contractuele uren aan personen met een arbeidsovereenkomst in het kwartaal ten op zichte van het aantal personen met een arbeidsovereenkomst in het kwartaal. Een persoon telt naar rato mee voor de periode dat hij/zij personeelslid is in het kwartaal.

Deze indicator wordt berekend op organisatieniveau, per vestiging en per zorg/niet-zorg gerelateerde functies. Ook worden per categorie totalen berekend.

Op de (deel)resultaten van deze indicator kan een verdeelsleutel worden toegepast.


## Uitgangspunten

* Alle personeelsleden (zorg- en niet-zorg) worden geïncludeerd.
* Toebedeling van contractuele uren naar een vestiging is op basis van de vestiging in de arbeidsovereenkomst.


## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle arbeidsovereenkomsten in het kwartaal.  
2. Bepaal o.b.v. de functie in de arbeidsovereenkomst(en) uit stap 1 of de functie zorg of niet-zorg gerelateerd was.
3. Bepaal per arbeidsovereenkomst de vestiging, het aantal contractuele uren en de duur van de overeenkomst in het kwartaal.
4. Tel de uitkomsten van de berekeningen uit stap 4 bij elkaar op en groepeer per vestiging, per vestiging voor zorg- en niet-zorg en voor de totale organisatie.
5. Bereken per vestiging en in totaal het aantal contractuele uren voor zorg en niet-zorggerelateerd.
6. Bereken per vestiging en in totaal het aantal dagen dat alle arbeidsovereenkomsten duurden voor zorg en niet-zorggerelateerd.
7. Deel het resultaat van stap 5 door het resultaat van stap 6.

**Kwartaal: dd-mm t/m dd-mm**
| Aantal contractuele uren per arbeidsovereenkomst: |  zorggerelateerd  | Niet-zorggerelateerd | 
|--------------------|--------|------------|
| Totaal organisatie | Stap 7 | Stap 7     | 
| Vestiging 1        | Stap 7 | Stap 7     | 
| Vestiging 2        | Stap 7 | Stap 7     | 
| Vestiging N        | Stap 7 | Stap 7     | 

