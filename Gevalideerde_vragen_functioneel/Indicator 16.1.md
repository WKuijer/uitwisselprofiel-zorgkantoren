---
title: 16.1. Aantal cliënten met het syndroom van Korsakov
---
## Indicator

**Definitie:** Het aantal cliënten met het syndroom van Korsakov dat bij een zorgaanbieder in zorg is.

**Teller:** Aantal cliënten met het syndroom van Korsakov.

**Noemer:** Niet van toepassing.

## Toelichting
Deze indicator geeft het aantal cliënten bij een zorgaanbieder op een bepaald moment aan. Het betreft cliënten met het syndroom van Korsakov.

Deze indicator wordt op een peildatum op organisatieniveau en per vestiging berekend. 


## Uitgangspunten

Geen


## Berekening

Deze indicator wordt als volgt berekend:
1. Selecteer op de peildatum alle cliënten met het syndroom van Korsakov.
2. Bepaal per cliënt de vestiging.
3. Bereken op basis van stap 2 per vestiging en voor de totale organisatie het aantal cliënten met het syndroom van Korsakov op de peildatum.

Peildatum: dd-mm-jjjj
| Organisatieonderdeel | Aantal cliënten met het syndroom van Korsakov | 
|---|---|
| Totaal organisatie | Stap 3 | 
| Vestiging 1 | Stap 3 | 
| Vestiging 2 | Stap 3 | 
| Vestiging N | Stap 3 |

