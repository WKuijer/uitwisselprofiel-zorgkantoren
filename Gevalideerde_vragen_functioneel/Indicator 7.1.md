---
title: 7.1. Aantal ingezette uren personeel per cliënt
---
## Indicator

**Definitie:** Het aantal ingezette uren van zorg gerelateerde personeelsleden per vestiging per kwartaal ten op zichte van het aantal cliënten per vestiging per kwartaal. Ingezette uren zijn inclusief meeruren, overuren en oproepuren.

**Teller:** Aantal ingezette uren van zorg gerelateerde personeelsleden met een arbeidsovereenkomst.

**Noemer:** Aantal cliënten met een Wlz-indicatie met zorgprofiel VV4 tot en met VV10.

## Toelichting
Deze indicator betreft het aantal geregistreerde uren van zorg gerelateerde personeelsleden ten opzichte van het aantal cliënten.

Deze teller betreft het aantal geregistreerde uren van personeelsleden in loondienst, dat zorgverlener is en binnen de verslagperiode over een arbeidsovereenkomst beschikte. Op basis van deze uren kan het aantal ingezette fte’s van deze personeelsleden berekend worden. Iedere afnemer van gegevens kan op basis van het totaal aantal uren zijn/haar eigen definitie van fte toepassen, zoals 36, 38 of 40 uur per week of 1692 uren per jaar. Voor de berekening van uren worden meeruren, overuren en oproepuren meegerekend.

De noemer betreft het totaal aantal geregistreerde cliënten met een Wlz-indicatie met zorgprofiel VV4 t/m VV10 in het kwartaal.

Deze indicator wordt op concernniveau en per vestiging berekend, voor zorg gerelateerd personeel. De verslagperiode betreft een kwartaal.

Op de (deel)resultaten van de teller van deze indicator kan een verdeelsleutel worden toegepast. Op de (deel)resultaten van de noemer van deze indicator dient geen verdeelsleutel te worden toegepast. Immers, de noemer betreft uitsluitend clienten met een Wlz-indicatie met een zorgprofiel VV4 tot en met VV10


## Uitgangspunten

* Per vestiging en voor de gehele organisatie.
* Personen die tegelijkertijd een of meerdere arbeidsovereenkomsten hebben tellen mee in de berekening wanneer de uren geregistreerd zijn als zorgverlener (o.b.v. de functie in de arbeidsovereenkomst).
* Om deze indicator te kunnen berekenen dienen de uren beschikbaar te zijn vanuit de urenregistratie.
* De geregistreerde ingezette uren, zijn uren exclusief verzuim en (zwangerschaps-)verlof en inclusief meeruren, overuren en oproepuren.
* Toebedeling van ingezette uren naar een vestiging is op basis van de vestiging in de urenregistratie. Indien deze niet beschikbaar zijn, worden de uren toebedeeld aan de vestiging gerelateerd aan de arbeidsovereenkomst. 
* Voor de berekening van fte, wordt rekening gehouden met 5 weken verlof. Alle gewerkte uren per jaar worden bij elkaar opgeteld en gedeeld door 47 (weken) om de uren per week te berekenen. Op basis van de gewenste eenheid (bijv. 36 uur/week) wordt het aantal fte berekend. In geval van 1 fte van 36 uur/week, wordt dus geacht 47 weken * 36 uur/week = 1692 uren per jaar te werken.


## Berekening

Deze indicator wordt als volgt berekend:
1.	Selecteer alle personeelsleden in loondienst die (o.b.v. hun functie) zorgverlener zijn.
2.	Tel per persoon het aantal uren (gewerkte tijd in uren) die de persoon heeft geregistreerd in de verslagperiode per vestiging.
3.	Bereken het aantal uren per vestiging en in totaal door alle geselecteerde uren van alle geselecteerde personen per vestiging bij elkaar op te tellen.
4. Selecteer alle cliënten die in het kwartaal zorg ontvangen op de betreffende vestiging. 
5. Bereken op basis van het resultaat uit stap 4 het aantal cliënten per vestiging en voor de totale organisatie.
6. Bereken de indicator door het resultaat uit stap 3 te delen door het resultaat uit stap 5.

| Periode: | Ingezette uren | Aantal cliënten | Ingezette uren per cliënt | 
| --- | --- | --- | --- |
| Totaal organisatie | stap 3 | stap 5 | stap 6 |
| vestiging 1 | stap 3 | stap 5 | stap 6 |
| vestiging 2 | stap 3 | stap 5 | stap 6 |
| vestiging N | stap 3 | stap 5 | stap 6 |
