---
title: 17.1. Geplande toe- en afname van het aantal wooneenheden
---

NB: Deze vraag gaat over wooneenheden. Waar 'wooneenheden' staat kan ook 'personen' ingevuld worden (later, na afstemming dan waarschijnlijk 2 indicatoren). Indicatoren 15.1 tot en met 15.4 van de zorgkantoren beschrijven de **capaciteit** en de **bezetting** in wooneenhenden en personen. Voor deze indicatoren geldt het volgende. De **capaciteit** kan uitgedrukt worden in ‘aantallen wooneenheden’ of ‘aantallen cliënten’. Beide per vestiging. Deze aantallen worden bepaald o.b.v. de registratie in het ECD: aantal woonheden bij de OE en per wooneenheid het aantal personen. Het is niet mogelijk om capaciteit uit te drukken in aantallen cliënten (of personen) met bepaalde kenmerken, zoals PG of somatiek, zorgprofiel, etc. Dit geldt ook voor ‘partners’. De **bezetting** kan uitgedrukt worden in ‘aantallen wooneenheden waarin cliënten op dat moment verblijven’ of ‘aantallen cliënten met specifieke kenmerken', zoals indicatie, leveringsvorm of PG/somatiek (o.b.v. bv. diagnose). Deze indicator 17.1 beschrijft de **geplande capaciteit**. Ook deze wordt uitgedrukt in wooneenheden of personen zonder kenmerken.


## Definitie

**Definitie:** Geplande toe- en afname van het aantal wooneenheden per vestiging op een peildatum.

**Teller:** Aantal wooneenheden.

**Noemer:** Niet van toepassing.


## Toelichting
Door middel van deze uitvraag worden de mutaties gevraagd die tot een toe- of afname leiden van het aantal wooneenheden. Het betreft mutaties die vanaf 1-1-2021 gepland zijn. Met mutatie wordt de geplande realisatie van extra wooneenheden bedoelt. Dit kan bijvoorbeeld door de aanbouw of huur van een nieuw pand of de uitbreiding van een extramuraal zorgteam. Krimp betekent de afname van het aantal wooneenheden door bijvoorbeeld het sluiten van een locatie of het opheffen van een extramuraal zorgteam. 

Het gaat om de volgende informatie:
-  De mutaties in wooneenheden bij de zorgaanbieder die tot een toename van het aantal wooneenheden gaan leiden vanaf 1-1-2021. 
- De mutaties in wooneenheden bij de zorgaanbieder die tot een afname van het aantal wooneenheden gaan leiden vanaf 1-1-2021.

Het gaat om de mutaties per vestiging op een peildatum.


## Uitgangspunten

I. betreft definities KIK-V. 

II-IV. zijn overgenomen uit de regiomonitor. 


### I. Gehanteerde definities KIK-V
- De capaciteit van een vestiging en van de organisatie wordt uitgedrukt in het aantal wooneenheden of personen dat in een vestiging (en bij de organisatie als geheel) van een zorgaanbieder kan wonen.
- De capaciteit uitgedrukt in personen wordt bepaald op basis van het aantal personen dat in elke wooneenheid kan wonen.
- Het betreft wooneenheden die geschikt zijn voor bewoning door een of meerdere cliënten en/of een partner (of personen partners zijn, is onbekend).
- Een wooneenheid is het samenstel van één, of meerdere kamer of ruimtes (onz-g:Room), die samen geschikt zijn voor bewoning en geen kleinere wooneenheden bevat. Een wooneenheid betreft dus de kleinste eenheid geschikt voor bewoning. Een lokatie met meerdere wooneenheden wordt op zichzelf dus niet beschouwd als een wooneenheid, ook al biedt die lokatie wel de mogelijkheid om er te wonen. De nadruk bij dit concept ligt derhalve op het begrip ’–eenheid’.
- Er is sprake van een vpt-cluster (in een instelling) wanneer er ten minste één persoon met een Wlz-indicatie met zorgprofiel VV met leveringsvorm vpt en ten minste één persoon met een Wlz-indicatie met zorgprofiel VV met leveringsvorm verblijf op één vestiging wonen.


### II. Begrippen leveringsvormen in Regiomonitor	
- Intramuraal: Verblijf in een instelling met daarbij de zorg waarop de Wlz-klant is aangewezen. De Wlz-klant ontvangt zorg op basis van zijn/haar Zorgzwaartepakket. 
- MPT: Met het Modulair Pakket Thuis wordt de zorg in eigen woonomgeving van de Wlz-klant verleent.
- VPT niet-geclusterd: Volledig Pakket Thuis niet-geclusterd omvat de zorgverlening in de eigen woonomgeving van de Wlz-klant. Hierbij levert de zorgaanbieder de 'zorgcapaciteit' maar niet het wonen. 
- VPT geclusterd: Volledig Pakket Thuis geclusterd omvat de zorgverlening in geclusterde woonvormen of kleinschalige wooninitiatieven van meer dan 2 en minder dan 24 personen die langdurige zorg nodig hebben.


### III. Realisatiefases: intramuraal en VPT geclusterd	in Regiomonitor	
1. Conceptfase	Het strategisch vastgoedplan is vastgesteld door de Raad van Toezicht. Er is een uitgewerkte marktanalyse gedaan en het plan is verkennend besproken met externe stakeholders. Denk hierbij aan: gemeenten, financiers, corporaties, beleggers.
2. Initiatiefase	In deze fase wordt de projectorganisatie opgezet met de stakeholders. De contractvormen worden bepaald (aankoopgrond, huurovereenkomst), de bouwlocatie wordt gezocht en er vindt een toets ruimtelijke ordening plaats. De kosten die mogelijk een rol kunnen gaan spelen worden opgenomen in de stichtingskostenopzet en/of in budgetposten. De overall planning met mijlpalen is opgeleverd.
3. Definitiefase	Het functioneel en het technisch programma van eisen wordt in deze fase opgesteld en het structuurontwerp van het gebouw wordt gemaakt (voorloper van de ontwerpen in de ontwerpfase). Dit is ook de fase waarin de investerings-, vastgoedexploitatie- en de totale exploitatie kosten worden berekend. Voordat fase 4 start is de (haalbare) businesscase goedgekeurd door de Raad van Toezicht. 
4. Ontwerpfase	In deze fase wordt het voorlopig ontwerp, het definitief ontwerp en het technisch ontwerp opgesteld en opgeleverd. De gesprekken over ruimtelijke ordening vinden plaats met de gemeente. De bouwkosten worden geindiceerd en de definitieve prijs wordt opgenomen in de directiebegroting. In deze fase vindt ook de aanbesteding en contractvorming plaats. 
5. Voorbereiding- en realisatiefase	De bouw gaat van start, is gaande of is voltooid. In deze fase vindt ook het gereed maken van de inrichting en de faciliteiten binnen het pand plaats. 
6. Gerealiseerd	"De woningen zijn opgeleverd en het pand kan in gebruik worden genomen."
7. Geannuleerd	Het bouwplan is geannuleerd, de betreffende woningen zullen niet meer worden opgeleverd en de beoogde extra verpleegzorgplekken komen te vervallen.


### IV. Realisatiefases: MPT en VPT niet-geclusterd	in Regiomonitor	
1. Voorbereiding	Het plan om uitbreiding/krimp van verpleegzorgplekken te realiseren is door het bestuur geaccordeerd. In deze fase worden de voorbereidingen getroffen voor de uitbreiding/krimp
2. Gerealiseerd	De uitbreiding of krimp van het aantal verpleegzorgplekken is gerealiseerd
3. Geannuleerd	Het plan voor uitbreiding/krimp van verpleegzorgplekken is geannuleerd. 


## Berekening

NB: Berekening later uitwerken.

Peildatum: dd-mm-jjjj
| Naam vestiging | Gemeente | Geplande toename van het aantal wooneenheden | Geplande afname van het aantal wooneenheden | Fase | Beoogde datum ingebruikname |
| --- | --- | --- | --- | --- | --- |
| ... | ... | ... | ... | ... | ... |
