---
title: 14.2. Aantal cliënten per leveringsvorm
---
## Indicator

**Definitie:** Aantal cliënten per leveringsvorm op peildatum: per vestiging en in totaal met betrekking tot cliënten met een Wlz-indicatie met zorgprofiel VV.

**Teller:** Aantal cliënten per leveringsvorm.

**Noemer:** Niet van toepassing.

## Toelichting 
Deze informatievraag betreft per vestiging en voor de Organisatie als geheel het aantal cliënten per leveringsvorm op een peildatum. Het gaat om cliënten met een Wlz-indicatie met zorgprofiel VV4 t/m VV10.


## Uitgangspunten

* Personen met een Wlz-indicatie met zorgprofiel VV4 tot en met VV10 worden geïncludeerd.
* Per vestiging en voor de Organisatie als geheel.
* Op een peildatum.


## Berekening
Deze indicator wordt als volgt berekend:

1. Selecteer alle cliënten die op de peildatum zorg ontvangen en beschikken over een Wlz-indicatie met zorgprofiel VV4 tot en met VV10.
2. Bepaal voor alle cliënten uit stap 1 de leveringsvorm (Verblijf, VPT, MPT of PGB) en de vestiging.
3. Bepaal voor alle cliënten met leveringsvorm Verblijf uit stap 2 of dit met of zonder behandeling is.
4. Bereken op basis van stap 2 per vestiging en voor de organisatie als geheel het totaal aantal cliënten per leveringsvorm VPT, MPT en PGB.
5. Bereken op basis van stap 3 per vestiging en voor de organisatie als geheel het totaal aantal cliënten met en zonder behandeling voor de leveringsvorm Verblijf.
6. Bereken op basis van stap 2 en 3 de verhouding VPT t.o.v. Verblijf (met en zonder behandeling tezamen).

Datum: dd-mm-jjj

Organisatieonderdeel | Verblijf met <br>behandeling | Verblijf zonder <br>behandeling | VPT | MPT | PGB | Verhouding VPT/Verblijf <br>(met en zonder) behandeling |  
| --- | --- | --- | --- | --- | --- | --- | 
| Organisatie | Stap 5 | Stap 5 | Stap 4 | Stap 4 | Stap 4 | Stap 6 |
| Vestiging 1 | Stap 5 | Stap 5 | Stap 4 | Stap 4 | Stap 4 | Stap 6 |
| Vestiging 2 | Stap 5 | Stap 5 | Stap 4 | Stap 4 | Stap 4 | Stap 6 |
| Vestiging N | Stap 5 | Stap 5 | Stap 4 | Stap 4 | Stap 4 | Stap 6 |
