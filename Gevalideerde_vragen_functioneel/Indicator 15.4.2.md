---
title: 15.4.2 Aantal cliënten met een Wlz-indicatie per sector
---

## Indicator

**Definitie:** Het aantal cliënten met een Wlz-indicatie per sector per vestiging en op organisatieniveau op een peildatum.

**Teller:** Aantal cliënten met een Wlz-indicatie.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator geeft de bezetting van een zorgaanbieder op een peildatum per vestiging aan. Het betreft cliënten met een Wlz-inidcatie. 

Deze indicator wordt op een peildatum op organisatieniveau en per vestiging berekend.


## Uitgangspunten

Geen.

## Berekening

De verdeelsleutel wordt als volgt berekend:

1. Selecteer alle cliënten die in de verslagperiode beschikken over een Wlz-indicatie. 
2. Bepaal per cliënt uit stap 1 de vestiging(en) en de langdurige zorgsector(en) waaronder de Wlz-indicatie(s) valt of vallen. 
3. Bereken op basis van stap 2 per vestiging en voor de totale organisatie het aantal cliënten op de peildatum.

Peildatum: dd-mm-jjjj 
|Organisatieonderdeel  |  VV  | LG | LVG |  VG  | ZGAUD | ZGVIS | GGZ-B |
|----------------|--------|-----------|--------|--------|--------|-----------|--------|
| Totaal organisatie | Stap 3 | Stap 3    | Stap 3 |Stap 3| Stap 3 | Stap 3    | Stap 3 |
| Vestiging 1      | Stap 3 | Stap 3    | Stap 3 |Stap 3| Stap 3 | Stap 3    | Stap 3 |
| Vestiging 2      | Stap 3 | Stap 3    | Stap 3 |Stap 3| Stap 3 | Stap 3    | Stap 3 |
| Vestiging N      | Stap 3 | Stap 3    | Stap 3 |Stap 3| Stap 3 | Stap 3    | Stap 3 |

