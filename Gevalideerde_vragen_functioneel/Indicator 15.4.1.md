---
title: 15.4.1 Aantal bewoners per wet
---

## Indicator

**Definitie:** Het aantal cliënten per wet per vestiging en op organisatieniveau op een peildatum.

**Teller:** Aantal cliënten per wet.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator geeft de bezetting van een zorgaanbieder op een peildatum per vestiging aan. Het betreft cliënten die op de peildatum beschikken over een Wlz-indicatie en/of Zvw-traject en/of Wmo-beschikking of ander traject. 

Deze indicator wordt op een peildatum op organisatieniveau en per vestiging berekend.


## Uitgangspunten

Geen.

## Berekening

De indicator wordt als volgt berekend:

1. Selecteer alle clienten die in de verslagperiode beschikken over een Wlz-indicatie en/of Zvw-traject en/of Wmo-beschikking of overige trajecten. 
2. Bepaal per client uit stap 1 de vestiging(en) en de wet waaronder de Wlz-indicatie(s) en/of Zvw-trajecten en/of Wmo-beschikkingen en/of overige trajecten vallen. 
3. Bereken op basis van stap 2 per vestiging en voor de totale organisatie het aantal cliënten op de peildatum.

Peildatum: dd-mm-jjjj 
| Organisatieonderdeel | Wlz | Zvw | Wmo | Overig |
|----------------|--------|-----------|--------|----|
| Totaal organisatie | Stap 4 | Stap 4    | Stap 4 | Stap 4 |
| Vestiging 1      | Stap 4 | Stap 4    | Stap 4 | Stap 4 |
| Vestiging 2      | Stap 4 | Stap 4    | Stap 4 | Stap 4 |
| Vestiging N      | Stap 4 | Stap 4    | Stap 4 | Stap 4 |
