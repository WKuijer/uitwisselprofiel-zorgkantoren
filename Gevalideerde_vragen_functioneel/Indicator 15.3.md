---
title: 15.3. Aantal personen dat per vestiging kan wonen
---

## Indicator

**Definitie:** Het aantal personen dat bij een zorgaanbieder per vestiging en op organisatieniveau op een peildatum kan wonen. 

**Teller:** Aantal bewoners.

**Noemer:** Niet van toepassing.


## Toelichting
Deze indicator geeft de capaciteit van een zorgaanbieder op een bepaald moment aan. De capaciteit betreft per vestiging het aantal personen dat bij een zorgaanbieder kan wonen, oftewel bewoner. Een bewoner betreft bijvoorbeeld een cliënt of de partner van een cliënt. 

Deze indicator wordt op een peildatum op organisatieniveau en per vestiging berekend.


## Uitgangspunten

* De capaciteit van een vestiging en van de organisatie wordt uitgedrukt in het aantal personen dat in een vestiging (en bij de organisatie als geheel) van een zorgaanbieder kan wonen, oftewel bewoner.
* De capaciteit wordt bepaald op basis van het aantal personen dat in elke wooneenheid kan wonen.
* Een wooneenheid is het samenstel van één, of meerdere kamer of ruimtes (onz-g:Room), die samen geschikt zijn voor bewoning en geen kleinere wooneenheden bevat. Een wooneenheid betreft dus de kleinste eenheid geschikt voor bewoning. Een lokatie met meerdere wooneenheden wordt op zichzelf dus niet beschouwd als een wooneenheid, ook al biedt die lokatie wel de mogelijkheid om er te wonen. De nadruk bij dit concept ligt derhalve op het begrip ’–eenheid’.
* Indicatoren uit de reeks 15 en 16 in het uitwisselprofiel van de zorgkantoren beschrijven de capaciteit en de bezetting in wooneenhenden en personen. Voor deze indicatoren geldt het volgende. Capaciteit en bezetting worden niet gecombineerd in één indicator. De **capaciteit** kan uitgedrukt worden in ‘aantallen wooneenheden’ of ‘aantallen cliënten’. Beide per vestiging en op organisatieniveau. Deze aantallen worden bepaald o.b.v. de registratie in het Electronisch Cliënten Dossier: aantal woonheden bij een vestiging en per wooneenheid het aantal personen. Het is niet mogelijk om capaciteit uit te drukken in aantallen cliënten (of personen) met bepaalde kenmerken, zoals diagnose, zorgprofiel, etc. Dit geldt ook voor ‘partners’ van cliënten. De **bezetting** kan uitgedrukt worden in ‘aantallen wooneenheden waarin cliënten op dat moment verblijven’ of ‘aantallen cliënten met specifieke kenmerken', zoals indicatie, leveringsvorm of PG/somatiek (o.b.v. bv. zorgprofiel).


## Berekening

Deze indicator wordt als volgt berekend:

1. Bepaal op de peildatum per vestiging het aantal personen dat in de betreffende vestiging kan wonen (bewoners). 
2. Bereken het totaal aantal bewoners voor de organisatie.

Peildatum: dd-mm-jjjj 
| Organisatieonderdeel | Capaciteit aantal bewoners |  
|----------------|--------|
| Totaal organisatie | Stap 2 | 
| Vestiging 1 | Stap 1 | 
| Vestiging 2 | Stap 1 | 
| Vestiging N | Stap 1 | 
