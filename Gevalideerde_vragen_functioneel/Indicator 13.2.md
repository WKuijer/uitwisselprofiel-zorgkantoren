---
title: 13.2. Percentage uitstroom personeel
---
## Indicator

**Definitie:** Het percentage uitstroom betreft het aantal personen dat op de peildatum minus 1 jaar werkzaam was als zorggerelateerd personeelslid in loondienst bij het concern, maar dat niet is op de peildatum, gedeeld door het aantal zorggerelateerde personeelsleden in loondienst op de peildatum minus 1 jaar.

**Teller:** Aantal zorggerelateerde personeelsleden in loondienst op de peildatum minus 1 jaar dat niet werkzaam is als zorggerelateerd personeelslid op de peildatum.

**Noemer:** Aantal zorggerelateerde personeelsleden in loondienst op de peildatum minus 1 jaar.

## Toelichting

De indicator betreft het aantal zorggerelateerde personeelsleden in loondienst op de peildatum minus 1 jaar, en die op de peildatum niet meer in loondienst zijn of niet meer over een zorggerelateerde functie beschikken, ten op zichten van het (totaal) aantal zorggerelateerde personeelsleden op de peildatum minus 1 jaar. 

De teller van deze indicator betreft het aantal zorggerelateerde personeelsleden die op de peildatum minus 1 jaar zorggerelateerde personeelslid waren en een jaar later geen personeelslid in loondienst (meer) zijn of niet meer over een zorggerelateerde functie beschikken. 

De noemer betreft het het aantal zorggerelateerde personeelsleden in loondienst op de peildatum minus 1 jaar.

De indicator wordt berekend op concernniveau.

Op de (deel)resultaten van deze indicator kan een verdeelsleutel worden toegepast.


## Uitgangspunten

* Personen die tegelijkertijd meerdere arbeidsovereenkomsten hebben tellen (o.b.v. de functie in de arbeidsovereenkomst) maximaal 1 keer mee per zorggerelateerde en/of niet-zorggerelateerde functie.
* Per peildatum (elk kwartaal) wordt gekeken welke personen op de peildatum zijn uitgetsroomd ten op zichte van de personen een jaar eerder dan de peildatum.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle personen die op de peildatum minus 1 jaar personeelslid waren en beschikken over een zorggerelateerde functie.  
2. Bereken o.b.v. stap 1 het aantal personen voor het totale concern.
3. Selecteer o.b.v. stap 1 alle personen die op de peildatum geen personeelslid (meer) waren of niet meer beschikten over een zorggerelateerde functie.
4. Bereken o.b.v. stap 3 het aantal personen voor het totale concern, deel door de resultaten uit stap 3 en vermenigvuldig met 100%.

| Aantal personeelsleden op peildatum minus 1 jaar: | Totaal zorggerelateerd |  % zorggerelateerde uitstroom t.o.v. totaal zorggerelateerd |
|----------------|--------|-----------|
| Totaal concern | Stap 2 | Stap 4 |

