---
title: Release- en versiebeschrijving
weight: 3
---

| **Releaseinformatie** |  |
|---|---|
| Release | 1.0 |
| Versie | 1.0 is vastgesteld door de Ketenraad o.b.v. versie 0.9 |
| Doel | Release 1.0 betreft de informatiebehoefte van zorgkantoren naar informatie over personeel en cliënten in het inkoopproces. |
| Doelgroep | Zorgkantoren (CZ, DSW, ENO, Menzis, VGZ, Zilveren Kruis, Zorg & Zekerheid); Zorgaanbieders verpleeghuiszorg; Zorgverzekeraars Nederland|
| Totstandkoming | De ontwikkeling van release 1.0 is uitgevoerd door het programma KIK-V in samenwerking met zorgkantoren VGZ, Zilveren Kruis en Menzis, evenals Zorgverzekeraars Nederland. De uitwerking is getoetst bij en akkoord bevonden door de andere zorgkantoren CZ, DWS, ENO en Zorg en Zekerheid. Release 1.0 wordt vastgesteld door de Ketenraad KIK-V op basis van versie 0.9. |
| Inwerkingtreding | 1 januari 2023 |
| Operationeel toepassingsgebied | Monitoring contractafspraken tussen zorgkantoor en zorgaanbieders het gebied van personeel en cliënten |
| Status | Ter vaststelling door Ketenraad KIK-V |
| Functionele scope | Release 1.0 omvat basisinformatie personeel en cliënten ten behoeve van de implementatie |
| Licentie | Creative Commons: Naamsvermelding-GeenAfgeleideWerken 4.0 Internationaal (CC BY-ND 4.0). |