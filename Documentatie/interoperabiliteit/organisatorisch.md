---
title: Organisatorische interoperabiliteit
weight: 2
---
## Aanlevering

De resultaten dienen soms op een peildatum, soms over een kwartaal beschikbaar te zijn. Afhankelijk van de interne processen bij de zorgaanbieders zijn deze minimaal 1 dag na de gevraagde meetperiode (dag, kwartaal) beschikbaar. Per vraag is bekeken welke peildatum of peilperiode van een kwartaal wordt gehanteerd: zie Semantische laag voor verdere details.

## Gewenste momenten van terugkoppeling

De zorgkantoren zullen in 2023 in gesprek met de zorgaanbieders invullen geven aan de terug te koppelen informatie voor de zorgaanbieders, bijvoorbeeld als spiegelinformatie.

## Looptijd

De looptijd van het uitwisselprofiel is continu doorlopend tot het moment van wijziging.

## Gewenste bewaartermijn

Voor zorgkantoren is het wenselijk dat er historie bij een zorgaanbieder beschikbaar is ten behoeve van trendanalyse. Voor de trendanalyses die zorgkantoren doen, is historische data van maximaal 3 jaar relevant.

## Afspraken bij twijfels over de kwaliteit van gegevens

Als een zorgkantoor twijfels heeft over de interpretatie van de gegevens en eventuele twijfel over de kwaliteit van gegevens, wordt tijdens de reguliere gesprekken tussen zorginkoper/ kwaliteitsadviseur van het zorgkantoor en de betreffende zorgaanbieder de juiste context en toelichting bij de cijfers opgevraagd. 

## Afspraken over een eventuele mogelijkheid tot nalevering en correctie

De gegevens kunnen door de zorgaanbieder worden aangepast en het volgend kwartaal verbeterd worden klaargezet voor het zorgkantoor.

## In- en exclusiecriteria

De uitvraag is bedoeld voor organisaties die onder onderstaande inclusiecriteria vallen:

1. Doelgroep

- Tot de doelgroep behoren alle instellingen met een Wlz contract

- Tot de doelgroep behoren alle instellingen binnen een zorgkantoorregio afbakening (per concessiehouder)

2. Profiel (inclusiecriteria)

- De profielen conform [Wet langdurige zorg](https://wetten.overheid.nl/BWBR0036014/2021-05-12#BijlageA) worden geïncludeerd.
