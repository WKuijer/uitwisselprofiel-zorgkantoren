---
title: Juridische interoperabiliteit
weight: 1
---
## Grondslag

De grondslag voor deze gegevensuitwisseling is te vinden in de Wet langdurige zorg (Wlz).

## Doel van gegevensverwerking

Zie [Juridisch kader](https://kik-v-publicatieplatform.nl/afsprakenset/2.1.0/content/docs/juridisch-kader) rij 4 van de tabel, Wlz artikel 4.2.2 (met name b, f en g)

Het doel van deze gegevensuitwisseling is om als zorgkantoor controle te kunnen houden op het verloop, de beschikbaarheid van (juiste) personeel, het verzuim en of voldaan wordt aan afspraken die op het personele vlak met zorgaanbieders overeen gekomen zijn. Zorgkantoren werken hiervoor met Early Warning Scores (EWS). Deze systematiek verschilt per zorgkantoor want is afhankelijk van het inkoopbeleid en strategisch belangrijke thema’s van het betreffende zorgkantoor.