---
title: Interoperabiliteit
weight: 2
---
De sectie interoperabiliteit bestaat uit verschillende sub-onderdelen, namelijk:

* Sub-sectie 1: [Juridische interoperabiliteit](/Documentatie/interoperabiliteit/juridisch.md)
* Sub-sectie 2: [Organisatorische interoperabiliteit](/Documentatie/interoperabiliteit/organisatorisch.md)
* Sub-sectie 3: [Semantische interoperabiliteit](/Documentatie/interoperabiliteit/semantisch.md)
* Sub-sectie 4: [Technische interoperabiliteit](/Documentatie/interoperabiliteit/technisch.md)