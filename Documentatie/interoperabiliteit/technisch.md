---
title: Technische interoperabiliteit
weight: 4
---
Indien een zorgaanbieder kan berekenen en aanleveren via een datastation, dan kunnen die worden aangeleverd middels de KIK-starter. Voor meer informatie hierover zie [het Publicatieplatform](https://kik-v-publicatieplatform.nl/). De zorgaanbieder gebruikt hiervoor de volgende SPARQL queries: [klik hier](/Gevalideerde_vragen_technisch/)

Voor aanlevering middels de KIK-starter gelden de volgende afspraken:

· Voor verslagjaar 2022 kunnen gegevens worden aangeleverd via KIK-starter. Zorgaanbieder die zich hiervoor aanmelden krijgen van het programma KIK-V toegang tot de KIK-starter via een daarvoor ingericht aansluitproces.

· Voor gebruik van de KIK-starter worden geen kosten in rekening gebracht.