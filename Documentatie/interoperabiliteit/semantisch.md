---
title: Semantische interoperabiliteit
weight: 3
---
## Algemeen

| Gegeven | Doel en achtergrond |
| --- | --- |
| AGB code concern | Via de AGB code kunnen zorgkantoren declaraties koppelen aan het opgegeven NZA nummer en zo de productieaantallen controleren. |
| NZa-nummer, KvK nummer concern, KvK vestigingsnummers, CAK nummer, Naam concern, Naam vestigingen | Ter controle juiste zorgaanbieder |
| Contactpersoon zorgaanbieder | Indien er bij het zorgkantoor vragen zijn over hetgeen is aangeleverd, kan het zorgkantoor contact opnemen met dit contactpersoon. |

## Personeel

| **Vraag**                                                                                            |
|------------------------------------------------------------------------------------------------------|
| 1.1 Wat is het gemiddeld aantal personeelsleden?                                                     |
| 1.2 Wat is het aantal personeelsleden op een peildatum?                                              |
| 2.1 Wat is het aantal ingezette uren?                                                                |
| 2.2 Wat is het aantal verloonde uren?                                                                |
| 3.1 Wat is het percentage personeel met een arbeidsovereenkomst voor bepaalde tijd op een peildatum? |
| 4.1 Wat is de gemiddelde contractomvang van het personeel?                                           |
| 5.1 Wat is de leeftijdsopbouw van het personeel?                                                     |
| 6.1 Wat is het percentage ingezette uren personeel per kwalificatieniveau?                           |
| 7.1 Wat is het aantal ingezette uren personeel ingezet per cliënt?                                   |
| 8.1 Wat is het percentage ingezette uren personeel niet in loondienst (PNIL)?                        |
| 8.2 Wat is het percentage kosten personeel niet in loondienst (PNIL)?                                |
| 9.1 Wat is het aantal vrijwilligers?                                                                 |
| 10.1 Wat is het aantal leerlingen?                                                                   |
| 11.1 Wat is het kortdurend ziekteverzuimpercentage (excl. zwangerschapsverlof)?                      |
| 11.2 Wat is het kortdurend ziekteverzuimpercentage (incl. zwangerschapsverlof)?                      |
| 11.3 Wat is het langdurend ziekteverzuimpercentage (excl. zwangerschapsverlof)?                      |
| 11.4 Wat is het langdurend ziekteverzuimpercentage (incl. zwangerschapsverlof)?                      |
| 12.1 Wat is de verzuimfrequentie (excl. zwangerschapsverlof)?                                        |
| 12.2 Wat is de verzuimfrequentie (incl. zwangerschapsverlof)?                                        |
| 13.1 Wat is het percentage instroom personeel?                                                       |
| 13.2 Wat is het percentage uitstroom personeel?                                                      |
| 13.3 Wat is het percentage doorstroom personeel naar oplopend kwalificatieniveau?                    |
| 13.4 Wat is het percentage doorstroom personeel naar aflopend kwalificatieniveau?                    |

**Doel en achtergrond**

1. Zorgkantoren willen inzicht in o.a. personeelsopbouw, verloop en ziekteverzuim, omdat de ontwikkelingen op de arbeidsmarkt van groot belang zijn.
2. Voor zorgkantoren is het van belang om te weten of ze het leveren van zorg kunnen blijven garanderen of dat er actie moet worden ondernomen.
3. Zorgkantoren willen zorgaanbieders met elkaar kunnen vergelijken in een benchmark. Op die manier kunnen zij minder presenterende zorgaanbieders koppelen aan beter presterende zorgaanbieders, zodat zij van elkaar kunnen leren.

## Cliënten

| Vraag |
| --- |
| 14.1 Wat is het aantal cliënten per zorgprofiel? |
| 14.2 Wat is het aantal cliënten per leveringsvorm? |
| 14.3 Wat is het aantal cliënten per zorgprofiel per leveringsvorm? |

**Doel en achtergrond**

1. Deze indicatoren schetsen iets over het profiel van de cliënten die er wonen, hoe deze zorg gefinancierd wordt en hoe zwaar de zorg is (want de indicatie geeft de zorgzwaarte aan).
2. Met en zonder behandeling laat zien hoe de zorg georganiseerd is en waar verantwoordelijkheden liggen (de huisarts of specialist ouderengeneeskunde). Hierin worden vaak grote kwaliteitsverschillen gemerkt en daarom is dit een belangrijk aandachtspunt.

## Definities informatievragen

### Algemene uitgangspunten

Voor de berekening van de indicatoren en informatievragen in de verschillende uitwisselprofielen worden algemene uitgangspunten gehanteerd. Uitgangspunten die gelden voor specifieke indicatoren of informatievragen worden bij de voorbeeldberekening van de betreffende indicator beschreven.

Indicator-specifieke uitgangspunten gaan voor op algemene uitgangspunten van een uitwisselprofiel. Indicator-specifieke uitgangspunten staan beschreven in de functionele beschrijving van een informatievraag of indicator. De algemene uitgangspunten per uitwisselprofiel staan hier beschreven:

_1. De toepassing van jaar, kwartaal en/of maand_

Kalenderdagen worden bepaald op basis van 1 jaar = 12 maanden, 1 maand = 30 dagen, 1 kwartaal = 90 dagen. Elke maand en elk kwartaal telt daarmee even zwaar mee in een berekening.

2\. _Uren omrekenen naar fte_

Voor indicatoren en informatievragen waar het aantal fte berekend wordt geldt het volgende. Bij deze indicatoren worden het aantal uren berekend. Iedere afnemer van gegevens kan op basis van het totaal aantal uren zijn/haar eigen definitie van fte of jaaruren toepassen, zoals 36, 38 of 40 uur per week of 1692 uur per jaar. Een afnemer kan op deze manier zelf het aantal fte berekenen.

3\. _Relaties tussen overeenkomsten, rollen en groepen_

Voor de berekeningen met werkovereenkomsten en/of medewerkers worden de volgende definities en relaties gehanteerd: [klik hier](https://purl.org/ozo/onz-pers)

4\. _Zorgverleners en niet-zorgverleners indelen op basis van functie medewerker_

De zorgaanbieder bepaalt op basis van de functie van een medewerker of de betreffende persoon behoort tot de groep zorgverleners of juist niet (ook wel zorg-gerelateerde en niet-zorggerelateerde medewerkers genoemd).

Hierbij wordt de volgende werkwijze gehanteerd:

a. De zorgaanbieder bepaalt welke functies zorgverlener zijn (de overige functies behoren dan tot de groep ‘niet-zorgverlener’).

b. Of een persoon een zorgverlener (of juist niet) is, wordt bepaald op basis van de functie van die persoon gedurende een bepaalde periode.

c. Dit betreft de functie die vermeld staat in de werkovereenkomst van de werknemer.

d. Functies die bijvoorbeeld kunnen vallen onder de definitie van zorgverlener zijn: behandelaren, verpleegkundigen, verzorgenden, helpenden, geestelijk verzorgenden, gastvrouwen, vrijwilligerscoördinatoren, activiteitencoördinatoren, welzijnsmedewerkers, medewerkers activiteitenbegeleiding, beweegagogen, sociaal agogen, leerlingen, medewerkers leefplezier, woonbegeleiders, medewerkers zorg & welzijn, zij-instromers met BBL-opleiding, stagiaires, huiskamermedewerkers, SPW-ers, familiecoaches, voedingsassistenten die direct werken met klanten, huishoudelijke medewerkers of facilitaire medewerkers die direct werken met klanten, catering medewerkers die direct werken met klanten, locatiemanagers en teamleiders en anderen als ze (deels) werken als zorgpersoneel.

e. Nota bene: De aanwezigheid en/of hoeveelheid direct en indirect cliënt-contact is niet bepalend voor de selectie van functies. Of en in hoeverre de aanwezigheid en/of hoeveelheid cliënt-contact wordt meegewogen in de selectie van functies die als zorgverlener worden aangemerkt, is aan de zorgaanbieder zelf.

NB: De zorgaanbieder dient hiervoor een lijst met de indeling van de functies op te stellen (en te onderhouden). Bijvoorbeeld:

* functie: specialist ouderen geneeskunde
* zorgverlener?: ja

5\. _Kwalificatieniveau bepalen op basis van functie medewerker_

a. Het kwalificatieniveau wordt bepaald op basis van de functie die geregistreerd is bij de werkovereenkomst.

b. Elke functie is ingedeeld in maximaal één kwalificatieniveau.

c. Een kwalificatieniveau kan meerdere functies bevatten.

d. De indeling van functies in kwalificatieniveaus vindt per uitwisselprofiel als volgt plaats:

De zorgaanbieder bepaalt in welk kwalificatieniveau een functie wordt ingedeeld. Dit kan op basis van de leidraad die door de IGJ is voorgesteld. De indeling vindt plaats in een van de volgende 9 kwalificatieniveaus:

* Kwalificatieniveau 1;

* Kwalificatieniveau 2;

* Kwalificatieniveau 3;

* Kwalificatieniveau 4;

* Kwalificatieniveau 5;

* Kwalificatieniveau 6;

* Behandelaren/(para)medisch;

* Overig zorgpersoneel;

* Leerlingen.

De indeling van zorgverleners in een kwalificatieniveau wordt bepaald op basis van de functie in de werkovereenkomst. Om te bepalen in welk kwalificatieniveau een functie ingedeeld dient te worden, kan onderstaand overzicht als leidraad gebruikt worden:

* Kwalificatieniveau 1: zorghulp/ zorg-assistent;

* Kwalificatieniveau 2: helpende (inclusief 2+: helpende met extra certificaat om medicatie te mogen delen);

* Kwalificatieniveau 3: verzorgende (IG);

* Kwalificatieniveau 4: verpleegkundige op mbo-niveau;

* Kwalificatieniveau 5: verpleegkundige op hbo-niveau;

* Kwalificatieniveau 6: verpleegkundige op hbo-niveau;

* Behandelaren/(para)medisch

* Overig zorgpersoneel

* Leerlingen

6\. _Cliënten indelen in een financieringsstroom_

Cliënten zijn gedurende een bepaalde periode in zorg die volgt uit een Wlz-indicatie, (Zvw) DBC, (Wmo) beschikking. Er zijn meerdere verdeelsleutels financieringsstroom beschikbaar o.a. op basis van cliëntdagen en op basis van declaraties. De verdeelsleutels financieringsstroom staan hier: [klik hier](https://kik-v-publicatieplatform.nl/documentatie/Verdeelsleutels/)

7\. _Cliënten indelen in een Wlz langdurige zorgsector_

Voor de zorgkantoren wordt gewerkt met de volgende Wlz indicaties: Cliënten met indicaties in de volgende Wlz-langdurige zorgsectoren: VV; VG; LVG; LG; ZGaud; ZGvis; GGZ-B; GGZ-Wonen. De berekening van de verdeelsleutel Wlz langdurige zorgsector staat hier: [Klik hier](https://kik-v-publicatieplatform.nl/documentatie/Verdeelsleutels/)

8\. _Gebruik van de organisatiestructuur_

De organisatie van een zorgaanbieder is ingedeeld in verschillende (organisatie-)onderdelen, zoals een team, afdeling, een organisatorische eenheid en/of kostenplaats. Deze onderdelen kunnen ook naast elkaar bestaan.

Een cliënt is (bijvoorbeeld in het ECD) geregistreerd op een bepaald onderdeel van de organisatie. De cliënt is in zorg bij dit onderdeel.

De toerekening vindt als volgt plaats:

a. De zorgaanbieder bepaalt hoe de organisatie administratief is ingedeeld. Dit kan bijvoorbeeld op basis van kostenplaatsen en/of organisatorische eenheden (OE). Op basis van deze indeling ontstaat vervolgens een kostenplaats- en/of OE-structuur.

b. De zorgaanbieder bepaalt op basis van (een van) deze structuren welke OE’s en of/kostenplaatsen een van de volgende organisatieniveaus zijn:

· Concern;

· Vestiging;

Deze organisatieniveaus kunnen de volgende eigenschappen bevatten:

· KvK-nummer;

· Kostenplaats (in geval van een OE-structuur);

· Organisatorische Eenheid (in geval van een kostenplaatsstructuur).

Per OE of kostenplaats wordt bepaald of de te berekenen indicator of informatievraag te berekenen is met de gegevens die geregistreerd zijn op die betreffende OE of kostenplaats.

Bijvoorbeeld: voor het berekenen van het aantal uren dat door medewerkers is besteed per client is het noodzakelijk dat zowel de cliënten als de uren geregistreerd zijn op de betreffende OE.

Voor een aantal indicatoren worden de resultaten berekend per vestiging. De zorgkantoren spreken hier ook wel over locatie. In de model gegevensset wordt hiervoor vestiging gebruikt zoals deze is geregistreerd bij de KvK.

9\. _Peildatum, meetmoment en meetperiode_

a. De peildatum betreft de dag op basis waarvan de indicator berekend wordt.

Bijvoorbeeld: Het aantal medewerkers met een arbeidsovereenkomst op 1 januari (peildatum) betreft het aantal medewerkers dat op 1 januari beschikte over een arbeidsovereenkomst.

b. Het meetmoment betreft het moment waarop een meting wordt gedaan en de indicator of informatievraag wordt berekend met de op dat moment beschikbare gegevens.

Bijvoorbeeld: Het aantal medewerkers met een arbeidsovereenkomst op 1 januari (peildatum) kan berekend worden op 1 februari (meetmoment).

c. De meetperiode betreft de periode waarover de indicator of informatievraag wordt berekend.

Bijvoorbeeld: Het aantal medewerkers dat in december (meetperiode) in dienst treedt kan berekend worden op 1 februari (meetmoment).

### Indicatoren zorgkantoren

De functionele beschrijving van de berekening per indicator staat hier: [Klik hier](/Gevalideerde_vragen_technisch/)

Op alle indicatoren kunnen extra verdeelsleutels worden toegepast: één voor een verdeling per financieringsstroom (Wlz, Zvw, Wmo) en één voor een verdeling in langdurige zorgsector (Wlz VV, GGZ-B, LG, LVG, VG, ZGAUD en ZGVIS). [Klik hier](https://kik-v-publicatieplatform.nl/documentatie/Verdeelsleutels/) voor de verdeelsleutels.

## Benodigde gegevenselementen

De concepten, eigenschappen en relaties die nodig zijn om de indicatoren zorgkantoren te beantwoorden staan hier: [klik hier](/Gevalideerde_vragen_technisch/Modelgegevensset).

## Aggregatieniveau

Het aggregatieniveau is voor alle indicatoren organisatie- en vestigingsniveau.

## Contextinformatie

In de reguliere gesprekscyclus tussen zorgkantoor en zorgaanbieder kan context / toelichting worden gegeven in het geval van afwijkingen/ afwijkende cijfers. Daarnaast kunnen ook verbeterplannen en kwaliteitsplannen en -verslagen voor contextinformatie worden gebruikt.