---
title: Algemeen
weight: 1
---
## Doel van de uitvraag

Zorgkantoren kopen periodiek (voor één of meerdere jaren) zorg in bij gecontracteerde Wlz zorgaanbieders. In de overeenkomst die wordt gesloten tussen zorgkantoor en zorgaanbieder worden afspraken gemaakt over monitoring en naleving van de gemaakte afspraken. Naast de reguliere gegevensstroom van indicatie- en declaratiegegevens wordt hiervoor gegevens vergaard via openbaar beschikbare bronnen dan wel extra uitvragen bij zorgaanbieders. Zorgkantoren houden met Early Warning Scores (EWS) de (dis)continuïteit van zorg per zorgaanbieder in de gaten. Personele en cliëntgegevens zijn onderdeel van deze signalering.
